module.exports = [
    {
        name: 'Team Hero',
        chars: [{
            id: 1,
            name: 'Shun\'ei',
            title: 'Amped-up Illusionist',
            thumb: './fighters_index_01.png',
            full: 'fighters_full_01.png'
        }, {
            id: 2,
            name: 'Meitenkun',
            title: 'Sleeping Prodigy',
            thumb: './fighters_index_02.png',
            full: 'fighters_full_02.png'
        }, {
            id: 3,
            name: 'Benimaru Nikaido',
            title: 'Shooting Star',
            thumb: './fighters_index_03.png',
            full: 'fighters_full_03.png'
        }]
    }, {
        name: 'Team Ash',
        chars: [{
            id: 28,
            name: 'Ash Crimson',
            title: 'Mocking Firelight',
            full: 'character_ash.png',
            thumb: './fighters_index_28.png',
        }, {
            id: 38,
            name: 'Elisabeth',
            title: 'Will of Light',
            full: 'character_elisabeth.png',
            thumb: './fighters_index_38.png',
        }, {
            id: 39,
            name: 'Kukri',
            title: 'Sharp-Tongued Sandman',
            full: 'character_kukri.png',
            thumb: './fighters_index_39.png',
        }]
    }, {
        name: 'Team Sacred Treasures',
        chars: [{
            id: 6,
            name: 'Kyo Kusanagi',
            title: 'Cleansing Fire',
            full: 'character_kyo.png',
            thumb: './fighters_index_06.png',
        }, {
            id: 4,
            title: 'Unshackled Instinct',
            full: 'character_iori.png',
            name: 'Iori Yagami',
            thumb: './fighters_index_04.png',
        }, {
            id: 7,
            name: 'Chizuru',
            title: 'Swift Shamaness',
            full: 'character_chizuru.png',
            thumb: './fighters_index_07.png',
        }]
    }, {
        name: 'Team K\'',
        chars: [{
            id: 32,
            name: 'K\'',
            title: 'Better than K',
            full: 'character_k.png',
            thumb: './fighters_index_32.png',
        }, {
            id: 33,
            name: 'Maxima',
            title: 'Human Weapon of Steel',
            full: 'character_maxima.png',
            thumb: './fighters_index_33.png',
        }, {
            id: 34,
            name: 'Whip',
            title: 'Queen of the Battlefield',
            full: 'character_whip.png',
            thumb: './fighters_index_34.png',
        }]
    }, {
        name: 'Team Rival',
        chars: [{
            id: 31,
            name: 'Isla',
            title: 'Dreaming Brilliance',
            full: 'character_isla.png',
            thumb: './fighters_index_31.png',
        }, {
            id: 30,
            name: 'Heidern',
            title: 'Hard-Boiled Assassin',
            full: 'character_heidern.png',
            thumb: './fighters_index_30.png',
        }, {
            id: 29,
            title: 'Hidden Wisdom',
            full: 'character_dolores.png',
            name: 'Dolores',
            thumb: './fighters_index_29.png',
        }]
    }, {
        name: 'Team Fatal Fury',
        chars: [{
            id: 10,
            title: 'Legendary Hungry Wolf',
            name: 'Terry Bogard',
            full: 'character_terry.png',
            thumb: './fighters_index_10.png'
        }, {
            id: 8,
            title: 'Human Weapon',
            name: 'Andy Bogard',
            full: 'character_andy.png',
            thumb: './fighters_index_08.png'
        }, {
            id: 5,
            title: 'Young Muay Thai Champ',
            name: 'Joe Higashi',
            full: 'character_joe.png',
            thumb: './fighters_index_05.png'
        }]
    }, {
        name: 'Team Art of Fighting',
        chars: [{
            id: 16,
            title: 'Invincible Dragon',
            name: 'Ryo Sakazaki',
            full: 'character_ryo.png',
            thumb: './fighters_index_16.png'
        }, {
            id: 17,
            title: 'Mightiest Tiger',
            name: 'Robert Garcia',
            full: 'character_robert.png',
            thumb: './fighters_index_17.png'
        }, {
            id: 12,
            title: 'The Beautiful Kick\'s Illusion',
            name: 'King',
            full: 'character_king.png',
            thumb: './fighters_index_12.png'
        }]
    }, {
        name: 'Team Orochi',
        chars: [{
            id: 11,
            full: 'character_yashiro.png',
            title: 'Dauntless and Decisive',
            name: 'Yashiro',
            thumb: './fighters_index_11.png'
        }, {
            id: 14,
            full: 'character_shermie.png',
            title: 'Coquettish',
            name: 'Shermie',
            thumb: './fighters_index_14.png'
        }, {
            id: 15,
            full: 'character_chris.png',
            title: 'Tempo Mixer',
            name: 'Chris',
            thumb: './fighters_index_15.png'
        }]
    }, {
        name: 'Team Super Heroine',
        chars: [{
            id: 23,
            full: 'character_athena.png',
            title: 'Eternal Psychic Idol',
            name: 'Athena Asamiya',
            thumb: './fighters_index_23.png'
        }, {
            id: 13,
            full: 'character_mai.png',
            title: 'Fascinating Kunoichi',
            name: 'Mai Shiranui',
            thumb: './fighters_index_13.png'
        }, {
            id: 9,
            full: 'character_yuri.png',
            title: 'Wild Flying Swallow',
            name: 'Yuri Sakazaki',
            thumb: './fighters_index_09.png'
        }]
    }, {
        name: 'Team Ikari',
        chars: [{
            id: 18,
            full: 'character_leona.png',
            title: 'Silent Soldier',
            name: 'Leona Heidern',
            thumb: './fighters_index_18.png'
        }, {
            id: 19,
            full: 'character_ralf.png',
            title: 'One Man Army',
            name: 'Ralf Jones',
            thumb: './fighters_index_19.png'
        }, {
            id: 20,
            full: 'character_clark.png',
            title: 'Tough & Cool',
            name: 'Clark Still',
            thumb: './fighters_index_20.png'
        }]
    }, {
        name: 'Team G.A.W.',
        chars: [{
            title: 'Restored Siberian Magnate',
            id: 27,
            full: 'character_antnov.png',
            name: 'Antonov',
            thumb: './fighters_index_27.png'
        }, {
            id: 25,
            full: 'character_ramon.png',
            title: 'Strongest Human',
            name: 'Ramon',
            thumb: './fighters_index_25.png'
        }, {
            id: 26,
            full: 'character_kod.png',
            title: 'Dino King Burning for Revenge',
            name: 'King of Dinosaurs',
            thumb: './fighters_index_26.png'
        }]
    }, {
        name: 'Team Krohnen',
        chars: [{
            id: 35,
            full: 'character_krohnen.png',
            title: 'Exceptional Esper',
            name: 'Krohnen',
            thumb: './fighters_index_35.png'
        }, {
            id: 37,
            full: 'character_kula.png',
            title: 'Ice Doll',
            name: 'Kula Diamond',
            thumb: './fighters_index_37.png'
        }, {
            id: 36,
            full: 'character_angel.png',
            title: 'Knockout Free Spirit',
            name: 'Ángel',
            thumb: './fighters_index_36.png'
        }]
    }, {
        name: 'Team Secret Agent',
        chars: [{
            id: 21,
            full: 'character_bluemary.png',
            title: 'JOINT CRUSHER',
            name: 'Blue Mary',
            thumb: './fighters_index_21.png'
        }, {
            id: 24,
            full: 'character_vanessa.png',
            title: 'Crimson Agent',
            name: 'Vanessa',
            thumb: './fighters_index_24.png'
        }, {
            id: 22,
            full: 'character_luong.png',
            title: 'Enchanting Beauty',
            name: 'Luong',
            thumb: './fighters_index_22.png'
        }]
    }
]