(() => {
    window.onload = () => {
        var selected = document.querySelector('#selected');
        var teamElem = document.querySelector('#team');
        var team = [];

        document.querySelectorAll('.char').forEach(elem => {
            elem.addEventListener('click', (event) => {
                var dataSet = event.target.parentNode.dataset;
                select(dataSet);
            });
        });

        const select = (dataSet) => {
            if (team.length < 3 && !team.map((char) => char.id).includes(dataSet.id)) {
                selected.innerHTML = `<img src="img/${dataSet.full}"/>
                <div class="textbox"><h3>${dataSet.name}</h3><h4>${dataSet.title}</h4><button>Add to team</button><div>`

                selected.querySelector('button').addEventListener('click', (event) => {
                    team.push(dataSet);
                    renderTeam();

                    if (team.length == 3) {
                        selected.innerHTML = '<button>Download team</button>'
                        selected.querySelector('button').addEventListener('click', (event) => {
                            html2canvas(teamElem, {backgroundColor: '#722f5d'}).then(function (canvas) {
                                document.body.appendChild(canvas);
                                canvas = document.querySelector('canvas');
                                image = canvas.toDataURL("image/png", 1.0).replace("image/png", "image/octet-stream");
                                console.log(image);
                                var link = document.createElement('a');
                                link.download = "my-image.png";
                                link.href = image;
                                link.click();
                                canvas.parentNode.removeChild(canvas);
                            });
                        })
                    }
                })
            } else {
                selected.innerHTML = `<img src="img/${dataSet.full}"/>
                <div class="textbox"><h3>${dataSet.name}</h3><h4>${dataSet.title}</h4></div>`
            }

        }

        const renderTeam = () => {
            teamElem.innerHTML = `${team.map(char => `<div class="teamchar"><img src="img/${char.full}"/><b>${char.name}</b></div>`).join('')}`
        }
    }
})();