const pug = require('pug');
const fs = require('fs').promises;
const fse = require('fs-extra');
const sass = require('sass');
const path = require('path');
const { inlineSource } = require('inline-source');

const data = require('./src/data');

(async () => {
  fs.mkdir('./.public', { recursive: true });
  await fs.writeFile('./.public/index.html', pug.renderFile('./src/index.pug', {
    filename: 'src/index.pug', data
  }));

  await fs.copyFile('./src/js.js', './.public/js.js');

  await fse.copy('./img', './.public/img');

  const CSS = sass.renderSync({
    file: 'src/style/style.scss',
    sourceMap: true,
    outFile: '.public/style.css',
    outputStyle: 'compressed',
  });

  await Promise.all([
    fs.writeFile('.public/style.css', CSS.css),
    fs.writeFile('.public/style.css.map', CSS.map),
  ]);

  const html = await inlineSource(path.resolve('.public/index.html'), {
    compress: true,
    ignore: ['png']
  });

  await fs.writeFile('.public/index.html', html);
})();
